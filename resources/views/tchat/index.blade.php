@extends('base')

@section('main')
<div class="col-sm-12">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div>
<div class="row">
  <div class="col-sm-12">
    <h1 class="display-3">Tchat</h1>    
    <table class="table table-striped">
      <tbody>
          @foreach($tchat as $message)
          <tr>
              <td>{{$message->text}}</td>  
          </tr>
          @endforeach
      </tbody>
    </table>
  </div>
</div>
<div>
  @include('tchat.create');
</div>
@endsection