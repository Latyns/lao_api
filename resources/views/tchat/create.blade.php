@extends('base')


<div class="row">
 <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Ajouter un message</h1>
  <div>
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('tchat.store') }}">
          @csrf
          <div class="form-group">    
              <label for="text">Text:</label><br>
              <textarea type="text" class="form-control" name="text"/></textarea>
          </div>
          <button type="submit" class="btn btn-primary-outline">Envoyer le message</button>
      </form>
  </div>
</div>
</div>
