<div class="card-body">
                    <form method="GET" action="modification">
                        @csrf

                        <div class="form-group row">
                            <label for="amelioration" class="col-md-4 col-form-label text-md-right">{{ __('Amélioration') }}</label>

                            <div class="col-md-6">
                                <select id="amelioration" type="" class="form-control @error('amelioration') is-invalid @enderror" name="amelioration" value="{{ old('amelioration') }}" required autocomplete="amelioration" autofocus>
                                    <option value="consommation_eau">Consommation d'eau</option>
                                    <option value="consommation_electricite">Consommation d'électricité</option>
                                    <option value="recyclage">Recyclage</option>
                                    <option value="alimentation">Alimentation</option>
                                    <option value="impact_carbon">Impact carbon</option>
                                </select>

                                @error('amelioration')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Choisir') }}
                                </button>
                            </div>
                        </div>
                    </form>
</div>