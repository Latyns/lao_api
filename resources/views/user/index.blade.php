@extends('base')

@section('main')
<div class="row">
<div class="col-sm-12">
    <h1 class="display-3">Utilisateurs</h1>    
  <table class="table table-striped">
    <thead>
        <tr>
          <td>Prénom</td>
          <td>Nom</td>
          <td>Email</td>
          <td>Ville</td>
          <td>Amélioration</td>
          <td colspan = 2>Actions</td>
        </tr>
    </thead>
    <tbody>
        @foreach($user as $individu)
        <tr>
            <td>{{$individu->prenom}}</td>
            <td>{{$individu->nom}}</td>
            <td>{{$individu->email}}</td>
            <td>{{$individu->ville}}</td>
            <td>{{$individu->amélioration}}</td>
            <td>
                <form action="{{ route('dashboard.destroy', $individu->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit">Supprimer</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
<div>
</div>
@endsection