<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tchat;

class TchatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tchat = Tchat::all();

        return view('tchat.index', compact('tchat'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tchat.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'text'=>'required',
        ]);

        $tchat = new Tchat([
            'text' => $request->get('text')
        ]);
        $tchat->save();
        return redirect('/tchat')->with('succès', 'Message envoyé!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tchat = Tchat::find($id);
        return view('tchat.edit', compact('tchat'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        {
        $request->validate([
            'text'=>'required'
        ]);

        $tchat = Tchat::find($id);
        $tchat->text =  $request->get('text');
        $tchat->save();

        return redirect('/tchat')->with('success', 'Message modifié!');
    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tchat = Tchat::find($id);
        $tchat->delete();

        return redirect('/tchat')->with('succès', 'Message supprimé!');
    }
}