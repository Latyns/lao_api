<?php

use Illuminate\Support\Facades\Route;


Route::get('/', function () {
    return view('welcome');
});

Route::resource('tchat', 'tchatController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('/profile', function(){
	return view('profile.index');
});

Route::get('/profile/amelioration', function(){
	return view('profile.amelioration');
});

Route::get('/profile/modification', 'Auth\RegisterController@update');

Route::resource('dashboard', 'UserController');
